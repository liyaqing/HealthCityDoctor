package com.wonders.healthcitydoctor.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by liyaqing on 2017/6/2.
 */

public class DateUtil {
//    public static long TimeCha(String preTime,Date currentTime){
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        long minutes=0;
//        long diff=0;
//        try
//        {
//            Date preTimeDate = df.parse(preTime);
//             diff = currentTime.getTime() - preTimeDate.getTime();//这样得到的差值是微秒级别
//            long days = diff / (1000 * 60 * 60 * 24);
//            long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
//             minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
////            System.out.println(""+days+"天"+hours+"小时"+minutes+"分");
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        return diff;
//    }
    public static long timeCha(String preTime, Date currentTime){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long minutes=0;
        long diff=0;
        long days=0;
        try
        {
            Date preTimeDate = df.parse(preTime);
             diff = currentTime.getTime() - preTimeDate.getTime();//这样得到的差值是微秒级别
             days = diff / (1000 * 60 * 60 * 24);
            long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
             minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
//            System.out.println(""+days+"天"+hours+"小时"+minutes+"分");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return minutes;
    }
    //把日期转为字符串
    public static String ConverToString(Date date)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return df.format(date);
    }
    //把字符串转为日期
    public static Date ConverToDate(String strDate) throws Exception
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.parse(strDate);
    }
    public static String ConverToStringDate(Date date)  {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        return df.format(date);
    }

    public static String getDate(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(new Date());
    }
    public static String getMonth(){
        DateFormat df = new SimpleDateFormat("yyyy-MM");
        return df.format(new Date());
    }

    public static String getFirstDayForMonth(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal_1= Calendar.getInstance();//获取当前日期
        cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
        return df.format(cal_1.getTime());
    }
    public static String getLastDayForMonth(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //获取前月的最后一天
        Calendar cale = Calendar.getInstance();
        cale.set(Calendar.DAY_OF_MONTH,0);//设置为1号,当前日期既为本月第一天
        return  df.format(cale.getTime());
    }
}
