package com.wonders.healthcitydoctor.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;


import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.util.DateUtil;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.util.SPUtils;
import com.wonders.healthcitydoctor.util.SystemBarTintManager;

import java.util.Date;

import me.yokeyword.fragmentation.SupportActivity;

/**
 * Created by liyaqing on 2017/5/5.
 */

public class BaseMainActivity extends SupportActivity {//SupportActivity
    public String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        initSystemBar(true, R.color.colorPrimary);
        super.onCreate(savedInstanceState);
        log(TAG + "------" + "onCreate");
    }

    /**
     * 使状态栏透明
     */
    private static void transparentStatusBar(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    protected void onStop() {
        super.onStop();

        log(TAG + "------" + "onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        log(TAG + "------" + "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        log(TAG + "------" + "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        log(TAG + "------" + "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        log(TAG + "------" + "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log(TAG + "------" + "onDestroy");
    }

    /**
     * 打印信息
     *
     * @param msg
     */
    public void log(String msg) {
        if (canLogMsg()) {
            LogUtil.i(msg);
        }
    }

    /**
     * 控制activity的全局信息，是否能打印
     *
     * @return
     */
    public boolean canLogMsg() {
        return true;
    }

    /**
     * 设置系统状态栏颜色
     *
     * @param flag            true启用系统状态栏颜色设置，false关闭
     * @param colorResourceId 颜色的id
     */
    public void initSystemBar(boolean flag, int colorResourceId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatus(flag);
        }

        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintResource(colorResourceId);
    }

    public void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    /**
     * 跳转到另一个activity，附带动画效果
     * @param clazz
     */
    protected void gotoActivity(Class clazz){
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
        //this.overridePendingTransition(R.anim.activity_or_fragment_enter, R.anim.activity_or_fragment_exit);
    }

    public boolean isRemoveCookie(){
        boolean isRemoveCookie=false;
        String currentTime= DateUtil.ConverToString(new Date());
        String preTime=(String) SPUtils.get(getApplicationContext(),getString(R.string.touchTime),"0000-00-00 00:00:00");
        long chaTime= DateUtil.timeCha(preTime,new Date());
        LogUtil.i("timecha",chaTime+","+preTime+","+currentTime);
        if (chaTime>=30){
            CookieSyncManager.createInstance(getApplicationContext());
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.removeSessionCookie();//移除
            SPUtils.put(getApplicationContext(),getString(R.string.cookie),"");
            SPUtils.put(getApplicationContext(),getString(R.string.username),"");
            SPUtils.put(getApplicationContext(),"password","");
            SPUtils.put(getApplicationContext(),getString(R.string.isSavaPass),"");
            SPUtils.put(getApplicationContext(),getString(R.string.isAutoLogin),"");
            isRemoveCookie=true;
        }
        SPUtils.put(getApplicationContext(),getString(R.string.touchTime),currentTime);
        return isRemoveCookie;
    }

//    @Override
//    public void onBackPressedSupport() {
//        super.onBackPressedSupport();
//        finish();
//    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//           finish();
//            return false;
//        }else {
//            return super.onKeyDown(keyCode, event);
//        }
//
//    }


}
