package com.wonders.healthcitydoctor.ui.webviewItem;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeWebViewClient;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.github.lzyzsd.jsbridge.DefaultHandler;
import com.wonders.healthcitydoctor.HtmlUrl;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.base.BaseMainActivity;
import com.wonders.healthcitydoctor.ui.login.LoginActivity;
import com.wonders.healthcitydoctor.ui.webview.WebviewActivity;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.util.SPUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PageItemActivity extends WebviewActivity {

    private String url = "";
    private String title = "";
    private String type = "";
    private String method;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_page_item);
//        ButterKnife.bind(this);
//        url = getIntent().getStringExtra("url");
//        title = getIntent().getStringExtra("title");
//        type = getIntent().getStringExtra("type");
//        
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        url = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        type = getIntent().getStringExtra("type");
        initTile();
    }

    @Override
    public void gotoWebView(String url, String title, String type) {
        super.gotoWebView(url, title, type);
        Bundle bundle=new Bundle();
        bundle.putString("url",url);
        bundle.putString("title",title);
        bundle.putString("type",type);
        Intent intent=new Intent(getApplicationContext(), PageItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
    @Override
    public void initTile(){
        super.initTile();
//        this.view=view;
            if (type==null){
                type="";
            }
            switch (type) {
                case "yygh":
                    setTitle( title, "", R.drawable.fdj);
//                    locationAndContactsTask();
                    break;
                case "yyghCollection"://预约收藏
                    method = "scFunction";
                    break;
                case "loginYs"://  登录
                    SPUtils.put(getApplicationContext(),getString(R.string.cookie),"");
                    SPUtils.put(getApplicationContext(), "password", "");
                    SPUtils.put(getApplicationContext(), getString(R.string.isSavaPass), "");
                    SPUtils.put(getApplicationContext(), getString(R.string.isAutoLogin), "");
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                    break;
//                case "userLocation"://  提供获取用户经纬度方法
//                    locationAndContactsTask();
//                    break;
                case "yyghScreen"://  预约挂号筛选
                    setTitle(title, "", R.drawable.sx);
                    method = "sxShowOrHide";
                    break;
                case "jkzxScreen"://  健康资讯筛选
                    setEditTitle("大家都在搜：刷爆朋友圈的健康谣言", R.drawable.arrow_right_right, 0);
                    break;
                case "wdqy"://  按钮跳转到签约历史
                    setTitle(title, "", R.drawable.paper);
                    method = "toQyls";
                    break;
                case  "jkzxCollection":
                    method = "scFunction";
                    break;
                //健康=========
                case "selfTAdd":
                    setTitle( title,"提交",0);
                    method="submit";
                    break;
                //个人=========
                case "gotoWeb":
                    setTitle( title,"",R.drawable.icon_family_add);
                    method="submit";
                    break;
                case "addFamily":
                    setTitle(title,"提交",0);
                    method="submit";
                    break;
                default:
                    setTitle( title);

            }
//        else {
//            if (title.equals("健康资讯")){
//                setEditTitle(view, "大家都在搜：刷爆朋友圈的健康谣言", R.drawable.arrow_right_right, 0);
//            }else {
//                setTitle(view, title);
//            }
//        }


    }
}
