package com.wonders.healthcitydoctor.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.wonders.healthcitydoctor.HtmlUrl;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.eventbus.StartBrotherEvent;
import com.wonders.healthcitydoctor.eventbus.TabSelectedEvent;
import com.wonders.healthcitydoctor.ui.base.BaseBackFragment;
import com.wonders.healthcitydoctor.ui.base.BaseMainFragment;
import com.wonders.healthcitydoctor.ui.yy.YyActivity;
import com.wonders.healthcitydoctor.ui.yy.YyFragment;
import com.wonders.healthcitydoctor.view.BottomBar;
import com.wonders.healthcitydoctor.view.BottomBarTab;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import me.yokeyword.fragmentation.SupportFragment;
import me.yokeyword.fragmentation.anim.DefaultNoAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public class MainFragment extends BaseMainFragment {

    @BindView(R.id.fl_tab_container)
    FrameLayout flTabContainer;
    @BindView(R.id.bottomBar)
    BottomBar bottomBar;

    private static final int REQ_MSG = 10;

    public static final int FIRST = 0;
    public static final int SECOND = 1;
    public static final int THIRD = 2;
    public static final int FOUR = 3;
    public static final int FIVE = 4;

    private SupportFragment[] mFragments = new SupportFragment[5];

    private BottomBar mBottomBar;


    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        if (savedInstanceState == null) {
            mFragments[FIRST] = HomeFragment.newInstance(HtmlUrl.reportUrl,"工作报告");
            mFragments[SECOND] = TreatmentFragment.newInstance(HtmlUrl.reserveUrl,"预约");
            mFragments[THIRD] = YlFragment.newInstance();
            mFragments[FOUR] = HealthFragment.newInstance(HtmlUrl.qyjmUrl,"签约居民");
            mFragments[FIVE] = PersonFragment.newInstance(HtmlUrl.personUrl,"个人中心");

            loadMultipleRootFragment(R.id.fl_tab_container, FIRST,
                    mFragments[FIRST],
                    mFragments[SECOND],
                    mFragments[THIRD],
                    mFragments[FOUR],
                    mFragments[FIVE]);
        } else {
            // 这里库已经做了Fragment恢复,所有不需要额外的处理了, 不会出现重叠问题

            // 这里我们需要拿到mFragments的引用,也可以通过getChildFragmentManager.getFragments()自行进行判断查找(效率更高些),用下面的方法查找更方便些
            mFragments[FIRST] = findChildFragment(HomeFragment.class);
            mFragments[SECOND] = findChildFragment(TreatmentFragment.class);
            mFragments[THIRD] = findChildFragment(YlFragment.class);
            mFragments[FOUR] = findChildFragment(HealthFragment.class);
            mFragments[FIVE] = findChildFragment(PersonFragment.class);
        }

        initView(view);
        return view;
    }


    private void initView(View view) {
        EventBus.getDefault().register(this);
        mBottomBar = (BottomBar) view.findViewById(R.id.bottomBar);

        mBottomBar
                .addItem(new BottomBarTab(_mActivity, R.drawable.bottom_grzx_blue, "工作报告"))
                .addItem(new BottomBarTab(_mActivity, R.drawable.bottom_yygl_gray, "预约"))
                .addItem(new BottomBarTab(_mActivity, R.drawable.bottom_yygl_gray, "养老"))
                .addItem(new BottomBarTab(_mActivity, R.drawable.bottom_hzgl_gray, "签约居民"))
                .addItem(new BottomBarTab(_mActivity, R.drawable.bottom_grzx_gray, "个人中心"));

        // 模拟未读消息
//        mBottomBar.getItem(FIRST).setUnreadCount(9);

        mBottomBar.setOnTabSelectedListener(new BottomBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, int prePosition) {


                BottomBarTab tab = mBottomBar.getItem(prePosition);
                if (position == SECOND) {
                    _mActivity.setFragmentAnimator(new FragmentAnimator(R.anim.anim_in,R.anim.anim_out));
//                    EventBus.getDefault().post(new StartBrotherEvent(YyFragment.newInstance()));
                    EventBus.getDefault().unregister(this);
                    startActivity(new Intent(getActivity(), YyActivity.class));

                    mBottomBar.setCurrentItem(prePosition);
                }else {
                    _mActivity.setFragmentAnimator(new FragmentAnimator(R.anim.h_fragment_enter,R.anim.h_fragment_exit));
                    showHideFragment(mFragments[position], mFragments[prePosition]);
                }
//                if (position == FIRST) {
//                    tab.setUnreadCount(0);
//                } else {
//                    tab.setUnreadCount(tab.getUnreadCount() + 1);
//                }
            }

            @Override
            public void onTabUnselected(int position) {

            }

            @Override
            public void onTabReselected(int position) {
                // 这里推荐使用EventBus来实现 -> 解耦
                // 在FirstPagerFragment,FirstHomeFragment中接收, 因为是嵌套的Fragment
                // 主要为了交互: 重选tab 如果列表不在顶部则移动到顶部,如果已经在顶部,则刷新
                EventBus.getDefault().post(new TabSelectedEvent(position));
            }
        });
    }

    @Override
    protected void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (requestCode == REQ_MSG && resultCode == RESULT_OK) {

        }
    }

    /**
     * start other BrotherFragment
     */
    @Subscribe
    public void startBrother(StartBrotherEvent event) {
        start(event.targetFragment);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
    @Override
    public boolean onBackPressedSupport() {
        _mActivity.finish();
        return true;
    }

}

