package com.wonders.healthcitydoctor.ui.main;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.wonder.sdklibrary.admin.MedicalSdkBuilder;
import com.wonder.sdklibrary.admin.MedicalSdkDirector;
import com.wonder.sdklibrary.admin.MedicalSdkImplBuilder;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.base.BaseMainFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YlFragment extends BaseMainFragment {

    @BindView(R.id.medical)
    FrameLayout medical;

    public YlFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static YlFragment newInstance() {
        YlFragment fragment = new YlFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_yl, container, false);
        ButterKnife.bind(this, view);
        MedicalSdkBuilder sdkBuilder = new MedicalSdkImplBuilder();
        MedicalSdkDirector sdkDirector = MedicalSdkDirector.getInstance(sdkBuilder);
        sdkDirector.setUserAppName("123").setUserKey("123").setUserChannel("123");

        FragmentManager fragmentManager =getActivity().getFragmentManager();
        FragmentTransaction transMedical = fragmentManager.beginTransaction();
        Fragment medical = sdkDirector.getMedicalFragment();
        transMedical.add(R.id.medical, medical).commit();
        return view;
    }
}
