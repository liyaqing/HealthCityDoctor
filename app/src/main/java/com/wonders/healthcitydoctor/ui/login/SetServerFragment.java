package com.wonders.healthcitydoctor.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wonders.healthcitydoctor.HtmlUrl;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.base.BaseBackFragment;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.util.SPUtils;


public class SetServerFragment extends BaseBackFragment {
    private Button setserver_btn;
    private EditText setserver_edittext;
    public static SetServerFragment newInstance() {
        SetServerFragment fragment = new SetServerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_set_server, container, false);
        findViewById(view);
        setListener();
        return attachToSwipeBack(view);
    }
    /**
     * 绑定界面UI
     */
    private void findViewById( View view) {
        setserver_btn = (Button)view.findViewById(R.id.setserver_btn);
        setserver_edittext = (EditText) view.findViewById(R.id.setserver_edittext);
        String serveraddress= HtmlUrl.hostIp;
        setserver_edittext.setText(serveraddress);

    }

    /**
     * UI事件监听
     */
    private void setListener() {
        // 登录按钮监听
        setserver_btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                HtmlUrl.hostIp=setserver_edittext.getText().toString().trim();
                LogUtil.i("setIp",HtmlUrl.hostIp);
                SPUtils.put(getContext().getApplicationContext(),getString(R.string.serverurl),HtmlUrl.hostIp);
                HtmlUrl.loginUrl= HtmlUrl.hostIp+HtmlUrl.loginPath;
                HtmlUrl.reportUrl= HtmlUrl.hostIp+HtmlUrl.reportPath;
                HtmlUrl.reserveUrl= HtmlUrl.hostIp+HtmlUrl.reservePath;
                HtmlUrl.qyjmUrl= HtmlUrl.hostIp+HtmlUrl.qyjmPath;
                HtmlUrl.personUrl= HtmlUrl.hostIp+HtmlUrl.personPath;

//				Toast.makeText(SetServerActivity.this,"HtmlUrl.hostIp1="+HtmlUrl.hostIp1,Toast.LENGTH_LONG).show();

                Toast.makeText(getContext().getApplicationContext(),"设置成功！重新打开", Toast.LENGTH_LONG).show();
//                Intent intent=new Intent(getContext().getApplicationContext(),LoginActivity.class);
//                intent.putExtra("succ","succ");
////                setResult(LoginActivity.REQCODE,intent);
                final Intent intent = getContext().getPackageManager().getLaunchIntentForPackage(getContext().getPackageName());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();

            }
        });


    }

}
