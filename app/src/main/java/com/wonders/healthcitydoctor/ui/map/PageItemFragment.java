package com.wonders.healthcitydoctor.ui.map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.eventbus.StartBrotherEvent;
import com.wonders.healthcitydoctor.ui.login.LoginActivity;
import com.wonders.healthcitydoctor.ui.webview.WebviewFragment;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

public class PageItemFragment extends WebviewFragment {
    private static final String URL = "URL";
    private static final String TITEL = "TITEL";
    private static final String TYPE = "TYPE";
    @BindView(R.id.webView)
    BridgeWebView webView;

    private String url="";
    private String titile="";
    private String type="";

    private String method;
    View view;
    public PageItemFragment() {
        // Required empty public constructor
    }

    public static PageItemFragment newInstance(String url, String titile, String type) {
        PageItemFragment fragment = new PageItemFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putString(TITEL, titile);
        args.putString(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            url = getArguments().getString(URL);
            titile = getArguments().getString(TITEL);
            type = getArguments().getString(TYPE);
            LogUtil.i("login===", titile + "," + url+","+type);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view=super.onCreateView(inflater, container, savedInstanceState);
        LogUtil.i("onCreateView===","homeFrageOnCreateView");
        return view;
    }


    @Override
    public void initTile(View view){
        super.initTile(view);
        this.view=view;
        if (type!=null) {
            switch (type) {
                case "yygh":
                    setTitle(view, titile, "", R.drawable.fdj);
                    locationAndContactsTask();
                    break;
                case "yyghCollection"://预约收藏
                    method = "scFunction";
                    break;
                case "login"://  登录
                    _mActivity.finish();
                    SPUtils.put(getContext().getApplicationContext(),getString(R.string.cookie),"");
                    SPUtils.put(getContext().getApplicationContext(),"password", "");
                    SPUtils.put(getContext().getApplicationContext(), getString(R.string.isSavaPass), "");
                    SPUtils.put(getContext().getApplicationContext(), getString(R.string.isAutoLogin), "");
                    startActivity(new Intent(getContext().getApplicationContext(), LoginActivity.class));
                    break;
                case "userLocation"://  提供获取用户经纬度方法
                    locationAndContactsTask();
                    break;
                case "yyghScreen"://  预约挂号筛选
                    setTitle(view, titile, "", R.drawable.sx);
                    method = "sxShowOrHide";
                    break;
                case "jkzxScreen"://  健康资讯筛选
                    setEditTitle(view, "大家都在搜：刷爆朋友圈的健康谣言", R.drawable.arrow_right_right, 0);
                    break;
                case "wdqy"://  按钮跳转到签约历史
                    setTitle(view, titile, "", R.drawable.paper);
                    method = "toQyls";
                    break;
                case  "jkzxCollection":
                    method = "scFunction";
                    break;
                //健康=========
                case "selfTAdd":
                    setTitle(view, titile,"提交",0);
                    method="submit";
                    break;
                //个人=========
                case "gotoWeb":
                    setTitle(view, titile,"",R.drawable.icon_family_add);
                    method="submit";
                    break;
                case "addFamily":
                    setTitle(view, titile,"提交",0);
                    method="submit";
                    break;
                default:
                    setTitle(view, titile);

            }
        }else{
            setTitle(view, titile);
        }
//        else {
//            if (titile.equals("健康资讯")){
//                setEditTitle(view, "大家都在搜：刷爆朋友圈的健康谣言", R.drawable.arrow_right_right, 0);
//            }else {
//                setTitle(view, titile);
//            }
//        }


    }
    @Override
    public void gotoWebView(String url, String title, String type) {
        super.gotoWebView(url, title,type);
//        start(PageItemFragment.newInstance(url, title, type));
            EventBus.getDefault().post(new StartBrotherEvent(PageItemFragment.newInstance(url, title, type)));
        if (this.type.equals("yygh")||this.type.equals("userLocation")){
            stopDW();
        }
    }

    @Override
    public void gotoMap(double latitude, double longitude, String hosName) {
        super.gotoMap(latitude, longitude,hosName);
        EventBus.getDefault().post(new StartBrotherEvent(MapFragment.newInstance(latitude, longitude,hosName)));
    }

    @Override
    public void setOnRightClickListener(View v) {
        super.setOnRightClickListener(v);
//            webView.loadUrl("javascript:"+method+"()");
        webView.callHandler(method, "", new CallBackFunction() {
            @Override
            public void onCallBack(String data) {
                LogUtil.i("method",method+","+data);
//                ToastMessageShort(data);
            }
        });
    }

    @Override
    public void setOnLeftClickListener(View v) {
        super.setOnLeftClickListener(v);
        pop();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    @Override
    public void loadScImg(String sfsc) {
        super.loadScImg(sfsc);
        if (sfsc.equals("1")){
            if (("yyghCollection").equals(type)||("jkzxCollection").equals(type))
            setTitle(view, titile,"",R.drawable.sc);
        }else {
            setTitle(view, titile,"",R.drawable.un_sc);
        }
    }

    @Override
    public void setOnEditClickListener(View v, String editText) {
        super.setOnEditClickListener(v, editText);
//        ToastMessageShort(editText);
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("condition",editText);
            LogUtil.i("condition",jsonObject.toString());
        }catch (JSONException e){
            e.printStackTrace();
        }

        webView.callHandler("queryData", jsonObject.toString(), new CallBackFunction() {
            @Override
            public void onCallBack(String data) {
                LogUtil.i("queryData",data+"");
//                ToastMessageShort(data);
            }
        });
    }
}
