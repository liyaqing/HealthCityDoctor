package com.wonders.healthcitydoctor.ui.main;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.webview.MainWebviewFragment;
import com.wonders.healthcitydoctor.ui.webviewItem.PageItemActivity;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.zxing.activity.CaptureActivity;

import java.util.List;

import butterknife.BindView;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class HealthFragment extends MainWebviewFragment implements  EasyPermissions.PermissionCallbacks{

    @BindView(R.id.webView)
    BridgeWebView webView;
    //打开扫描界面请求码
    private int REQUEST_CODE = 0x01;
    //扫描成功返回码
    private int RESULT_OK = 0xA1;
    private static final int RC_CAMERA_PERM = 123;
//    private static final int RC_LOCATION_CONTACTS_PERM = 124;

    private static final String URL = "URL";
    private static final String TITEL = "TITEL";
    private String url;
    private String titile;
    public HealthFragment() {
        // Required empty public constructor
    }

    public static HealthFragment newInstance(String url, String titile) {
        HealthFragment fragment = new HealthFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putString(TITEL, titile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL);
            titile = getArguments().getString(TITEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void gotoWebView(String url, String title, String type) {
        super.gotoWebView(url, title,type);
//       EventBus.getDefault().post(new StartBrotherEvent(PageItemFragment.newInstance(url,title,type)));
        Bundle bundle=new Bundle();
        bundle.putString("url",url);
        bundle.putString("title",title);
        bundle.putString("type",type);
        Intent intent=new Intent(getContext().getApplicationContext(), PageItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void initTile(View view) {
        super.initTile(view);
        setEditTitle(view,"搜索科室、医院、医生",0,R.drawable.plus);
    }
    // 你也可以使用简单的扫描功能，但是一般扫描的样式和行为都是可以自定义的，这里就写关于自定义的代码了
// 你可以把这个方法作为一个点击事件
//    public void customScan(){
//        new IntentIntegrator(this)
//                .setOrientationLocked(false)
//                .setCaptureActivity(CustomScanActivity.class) // 设置自定义的activity是CustomActivity
//                .initiateScan(); // 初始化扫描
//    }


    @Override
    public void setOnLeftClickListener(View v) {
        super.setOnLeftClickListener(v);
        LogUtil.i("camera=====1");
        cameraTask();

    }

    @Override
    public void setOnRightClickListener(View v) {


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //扫描结果回调
        if (resultCode == RESULT_OK) { //RESULT_OK = -1
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("qr_scan_result");
            //将扫描出的信息显示出来
            ToastMessageShort(scanResult);
        }
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(getContext().getApplicationContext(), R.string.returned_from_app_settings_to_activity, Toast.LENGTH_SHORT)
                    .show();
        }
    }
    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void cameraTask() {
        if (EasyPermissions.hasPermissions(getContext().getApplicationContext(), Manifest.permission.CAMERA)) {
            // Have permission, do the thing!
            LogUtil.i("camera=====2");
            try {
                Intent intent = new Intent(getContext().getApplicationContext(), CaptureActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
            }catch (SecurityException e){
                e.printStackTrace();
            }

        } else {
            LogUtil.i("camera=====3");
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_camera),
                    RC_CAMERA_PERM, Manifest.permission.CAMERA);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
        if (requestCode==RC_CAMERA_PERM){
            Intent intent = new Intent(getContext().getApplicationContext(), CaptureActivity.class);
            startActivityForResult(intent,REQUEST_CODE);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

}
