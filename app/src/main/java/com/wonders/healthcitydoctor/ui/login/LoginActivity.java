package com.wonders.healthcitydoctor.ui.login;

import android.os.Bundle;
import android.util.Log;


import com.wonders.healthcitydoctor.HtmlUrl;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.base.BaseMainActivity;
import com.wonders.healthcitydoctor.util.InputCleanLeakUtils;

import me.yokeyword.fragmentation.SupportFragment;
import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;
import me.yokeyword.fragmentation.helper.FragmentLifecycleCallbacks;

public class LoginActivity extends BaseMainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        Intent intent=getIntent();
//        String url=intent.getStringExtra("url");
//        String title=intent.getStringExtra("title");
//        String type=intent.getStringExtra("type");

        if (savedInstanceState == null) {
            loadRootFragment(R.id.fl_container, LoginFragment.newInstance(HtmlUrl.loginUrl,"登录",""));
        }

        // 可以监听该Activity下的所有Fragment的18个 生命周期方法
        registerFragmentLifecycleCallbacks(new FragmentLifecycleCallbacks() {

            @Override
            public void onFragmentSupportVisible(SupportFragment fragment) {
                Log.i("MainActivity", "onFragmentSupportVisible--->" + fragment.getClass().getSimpleName());
            }

            @Override
            public void onFragmentCreated(SupportFragment fragment, Bundle savedInstanceState) {
                super.onFragmentCreated(fragment, savedInstanceState);
            }
            // 省略其余生命周期方法
        });
    }

    @Override
    public void onBackPressedSupport() {
        // 对于 4个类别的主Fragment内的回退back逻辑,已经在其onBackPressedSupport里各自处理了
        super.onBackPressedSupport();
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        // 设置横向(和安卓4.x动画相同)
        return new DefaultHorizontalAnimator();
    }
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (ev.getAction()==MotionEvent.ACTION_UP) {
//            boolean isRemove = isRemoveCookie();
//            if (isRemove) {
//                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                finish();
//            }
//        }
//        return super.dispatchTouchEvent(ev);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        InputCleanLeakUtils.fixInputMethodManagerLeak(this);
    }
}
