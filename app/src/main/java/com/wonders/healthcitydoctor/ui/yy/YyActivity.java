package com.wonders.healthcitydoctor.ui.yy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.base.BaseMainActivity;
import com.wonders.healthcitydoctor.ui.login.LoginActivity;
import com.wonders.healthcitydoctor.ui.main.MainFragment;
import com.wonders.healthcitydoctor.util.InputCleanLeakUtils;

import me.yokeyword.fragmentation.SupportFragment;
import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;
import me.yokeyword.fragmentation.helper.FragmentLifecycleCallbacks;

public class YyActivity extends BaseMainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yy);
        if (savedInstanceState == null) {
            loadRootFragment(R.id.fl_container, YyFragment.newInstance());
//            replaceLoadRootFragment(R.id.fl_container, YyFragment.newInstance(),false);
        }

        // 可以监听该Activity下的所有Fragment的18个 生命周期方法
        registerFragmentLifecycleCallbacks(new FragmentLifecycleCallbacks() {

            @Override
            public void onFragmentSupportVisible(SupportFragment fragment) {
                Log.i("YyActivity", "onFragmentSupportVisible--->" + fragment.getClass().getSimpleName());
            }

            @Override
            public void onFragmentCreated(SupportFragment fragment, Bundle savedInstanceState) {
                super.onFragmentCreated(fragment, savedInstanceState);
            }
            // 省略其余生命周期方法
        });
    }

    @Override
    public void onBackPressedSupport() {
        // 对于 4个类别的主Fragment内的回退back逻辑,已经在其onBackPressedSupport里各自处理了
        super.onBackPressedSupport();
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        // 设置横向(和安卓4.x动画相同)
        return new DefaultHorizontalAnimator();
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction()== MotionEvent.ACTION_UP) {
            boolean isRemove = isRemoveCookie();
            if (isRemove) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        InputCleanLeakUtils.fixInputMethodManagerLeak(this);
    }
}
