package com.wonders.healthcitydoctor.ui.map;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.base.BaseBackFragment;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.util.QrUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QrCodeFragment extends BaseBackFragment {
    private static final String URL = "url";
    @BindView(R.id.img_qr)
    ImageView imgQr;
    private String url;

    public QrCodeFragment() {
        // Required empty public constructor
    }

    public static QrCodeFragment newInstance(String url) {
        QrCodeFragment fragment = new QrCodeFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_qr_code, container, false);
        ButterKnife.bind(this, view);
        setTitle(view,"我的二维码");
        LogUtil.i("ewm",url);
        Bitmap bitmap = QrUtil.createQRImage(url,300,300);
        imgQr.setImageBitmap(bitmap);
        return view;
    }
}
