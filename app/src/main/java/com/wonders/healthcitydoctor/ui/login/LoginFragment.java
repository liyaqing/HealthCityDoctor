package com.wonders.healthcitydoctor.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wonders.healthcitydoctor.HtmlUrl;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.eventbus.StartBrotherEvent;
import com.wonders.healthcitydoctor.ui.webview.MainWebviewFragment;
import com.wonders.healthcitydoctor.ui.webviewItem.PageItemActivity;
import com.wonders.healthcitydoctor.util.LogUtil;
import com.wonders.healthcitydoctor.util.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class LoginFragment extends MainWebviewFragment {
    private static final String URL = "URL";
    private static final String TITEL = "TITEL";
    private static final String TYPE = "TYPE";
    private String url;
    private String title;
    private String type;
    ImageView settingImg;
    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance(String url, String titile,String type) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putString(TITEL, titile);
        args.putString(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL);
            title = getArguments().getString(TITEL);
            type = getArguments().getString(TYPE);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=super.onCreateView(inflater, container, savedInstanceState);
        EventBus.getDefault().register(this);
        settingImg=(ImageView)view.findViewById(R.id.setting_imageview) ;
        settingImg.setVisibility(View.VISIBLE);
        setServiceUrl();
        initLoginInfo();
        settingImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new StartBrotherEvent(SetServerFragment.newInstance()));
            }
        });
        return view;
    }


    @Override
    public void gotoWebView(String url, String title, String type) {
        super.gotoWebView(url, title, type);
        LogUtil.i("login===", title + "," + url+","+type);
//            EventBus.getDefault().post(new StartBrotherEvent(PageItemFragment.newInstance(url, title, type)));
        Bundle bundle=new Bundle();
        bundle.putString("url",url);
        bundle.putString("title",title);
        bundle.putString("type",type);
        Intent intent=new Intent(getContext().getApplicationContext(), PageItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * 设置服务器地址
     */
    private void setServiceUrl() {
        String serveraddress = (String) SPUtils.get(getContext().getApplicationContext(), getString(R.string.serverurl), HtmlUrl.hostIp);
        HtmlUrl.loginUrl= serveraddress+ HtmlUrl.loginPath;
        HtmlUrl.reportUrl= serveraddress+HtmlUrl.reportPath;
        HtmlUrl.reserveUrl= serveraddress+HtmlUrl.reservePath;
        HtmlUrl.qyjmUrl= serveraddress+HtmlUrl.qyjmPath;
        HtmlUrl.personUrl= serveraddress+HtmlUrl.personPath;
    }
    @Subscribe
    public void startBrother(StartBrotherEvent event) {
        start(event.targetFragment);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
}
