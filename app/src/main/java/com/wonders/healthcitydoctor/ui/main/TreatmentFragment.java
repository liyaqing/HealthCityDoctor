package com.wonders.healthcitydoctor.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.ui.webview.MainWebviewFragment;

import butterknife.BindView;

public class TreatmentFragment extends MainWebviewFragment {


    @BindView(R.id.webView)
    BridgeWebView webView;
    private static final String URL = "URL";
    private static final String TITEL = "TITEL";
    private String url;
    private String titile;
    public TreatmentFragment() {
        // Required empty public constructor
    }

    public static TreatmentFragment newInstance(String url, String titile) {
        TreatmentFragment fragment = new TreatmentFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putString(TITEL, titile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL);
            titile = getArguments().getString(TITEL);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        EventBus.getDefault().post(new StartBrotherEvent(YyFragment.newInstance()));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

//    @Override
//    public void gotoWebView(String url, String title, String type) {
//        super.gotoWebView(url, title,type);
//        EventBus.getDefault().post(new StartBrotherEvent(PageItemFragment.newInstance(url,title,type)));
//    }

//    @Override
//    public void initTile(View view) {
//        super.initTile(view);
////        setTitle(view, "上海市普陀区中心医院", R.drawable.icon_mark_white, R.drawable.message);
//    }
}
