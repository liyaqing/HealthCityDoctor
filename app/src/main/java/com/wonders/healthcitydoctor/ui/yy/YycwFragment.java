package com.wonders.healthcitydoctor.ui.yy;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wonders.healthcitydoctor.ui.webview.MainWebviewFragment;
import com.wonders.healthcitydoctor.ui.webview.WebviewActivity;
import com.wonders.healthcitydoctor.ui.webviewItem.PageItemActivity;

public class YycwFragment extends MainWebviewFragment {
    private static final String URL = "URL";
    private static final String TITEL = "TITEL";
    private String url="";
    private String titile="";
    public YycwFragment() {
        // Required empty public constructor
    }
    public static YycwFragment newInstance(String url, String titile) {
        YycwFragment fragment = new YycwFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putString(TITEL, titile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL);
            titile = getArguments().getString(TITEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    @Override
    public void gotoWebView(String url, String title, String type) {
        super.gotoWebView(url, title,type);
        Bundle bundle=new Bundle();
        bundle.putString("url",url);
        bundle.putString("title",title);
        bundle.putString("type",type);
        Intent intent=new Intent(getContext().getApplicationContext(), PageItemActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
//        EventBus.getDefault().post(new StartBrotherEvent(PageItemFragment.newInstance(url,title,type)));
//        start(PageItemFragment.newInstance(url,title,type));
    }
}
