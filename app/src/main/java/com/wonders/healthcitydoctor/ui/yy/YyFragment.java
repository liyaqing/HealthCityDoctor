package com.wonders.healthcitydoctor.ui.yy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.wonders.healthcitydoctor.HtmlUrl;
import com.wonders.healthcitydoctor.R;
import com.wonders.healthcitydoctor.eventbus.StartBrotherEvent;
import com.wonders.healthcitydoctor.eventbus.TabSelectedEvent;
import com.wonders.healthcitydoctor.ui.base.BaseBackFragment;
import com.wonders.healthcitydoctor.ui.base.BaseMainFragment;
import com.wonders.healthcitydoctor.ui.main.HealthFragment;
import com.wonders.healthcitydoctor.ui.main.HomeFragment;
import com.wonders.healthcitydoctor.ui.main.PersonFragment;
import com.wonders.healthcitydoctor.ui.main.TreatmentFragment;
import com.wonders.healthcitydoctor.view.BottomBar;
import com.wonders.healthcitydoctor.view.BottomBarHorizontal;
import com.wonders.healthcitydoctor.view.BottomBarTab;
import com.wonders.healthcitydoctor.view.BottomBarTabHorizontal;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.yokeyword.fragmentation.SupportFragment;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public class YyFragment extends BaseMainFragment {

    @BindView(R.id.fl_tab_container)
    FrameLayout flTabContainer;
    @BindView(R.id.bottomBar)
    BottomBarHorizontal bottomBar;
    @BindView(R.id.img)
    ImageView img;
    public static final int FIRST = 0;
    public static final int SECOND = 1;
    public static final int THIRD = 2;
    private static final int REQ_MSG = 10;
    private SupportFragment[] mFragments = new SupportFragment[3];

    private BottomBarHorizontal mBottomBar;
    public YyFragment() {
        // Required empty public constructor
    }

    public static YyFragment newInstance() {
        YyFragment fragment = new YyFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_yy, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState == null) {
            mFragments[FIRST] = YyghFragment.newInstance(HtmlUrl.yyghUrl,"预约挂号");
            mFragments[SECOND] = YyjcFragment.newInstance(HtmlUrl.yyjcUrl,"预约检查");
            mFragments[THIRD] = YycwFragment.newInstance(HtmlUrl.yycwUrl,"预约床位");

                loadMultipleRootFragment(R.id.fl_tab_container, FIRST,
                        mFragments[FIRST],
                        mFragments[SECOND],
                        mFragments[THIRD]);
            } else {
                // 这里库已经做了Fragment恢复,所有不需要额外的处理了, 不会出现重叠问题

                // 这里我们需要拿到mFragments的引用,也可以通过getChildFragmentManager.getFragments()自行进行判断查找(效率更高些),用下面的方法查找更方便些
                mFragments[FIRST] = findChildFragment(YyghFragment.class);
                mFragments[SECOND] = findChildFragment(YyjcFragment.class);
                mFragments[THIRD] = findChildFragment(YycwFragment.class);
            }

            initView(view);

            return view;
        }

    private void initView(View view) {
        EventBus.getDefault().register(this);
        mBottomBar = (BottomBarHorizontal) view.findViewById(R.id.bottomBar);

        mBottomBar
                .addItem(new BottomBarTabHorizontal(_mActivity, R.drawable.blue_yygh, "预约挂号"))
                .addItem(new BottomBarTabHorizontal(_mActivity, R.drawable.gray_yyjc, "预约检查"))
                .addItem(new BottomBarTabHorizontal(_mActivity, R.drawable.gray_yycw, "预约床位"));

        // 模拟未读消息
//        mBottomBar.getItem(FIRST).setUnreadCount(9);

        mBottomBar.setOnTabSelectedListener(new BottomBarHorizontal.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, int prePosition) {
                showHideFragment(mFragments[position], mFragments[prePosition]);

            }

            @Override
            public void onTabUnselected(int position) {

            }

            @Override
            public void onTabReselected(int position) {
                // 这里推荐使用EventBus来实现 -> 解耦
                // 在FirstPagerFragment,FirstHomeFragment中接收, 因为是嵌套的Fragment
                // 主要为了交互: 重选tab 如果列表不在顶部则移动到顶部,如果已经在顶部,则刷新
                EventBus.getDefault().post(new TabSelectedEvent(position));
            }
        });
    }

    @Override
    protected void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        super.onFragmentResult(requestCode, resultCode, data);
        if (requestCode == REQ_MSG && resultCode == RESULT_OK) {

        }
    }

    /**
     * start other BrotherFragment
     */
    @Subscribe
    public void startBrother(StartBrotherEvent event) {
        start(event.targetFragment);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
    @Override
    public boolean onBackPressedSupport() {
        _mActivity.finish();
        return true;
    }


}
