package com.wonders.healthcitydoctor;

/**
 * Created by liyaqing on 2017/7/20.
 */

public class HtmlUrl {

//    public static String hostIp = "http://jkda.tunnel.echomod.cn";
//    public static String hostIp = "http://jkcsgw.tunnel.echomod.cn";
//    public static String hostIp = "http://jkcsapp.tunnel.echomod.cn";
//    public static String hostIp = "http://jkcsApp.tunnel.echomod.cn";
//    public static String hostIp = "http://jkcsjmh.tunnel.echomod.cn/";
//    public static String hostIp = "http://139.224.33.116:8780";
    public static String hostIp = "http://139.224.33.116:8888";
    public static String loginPath = "/html/ysd/login/loginYs.html";
    public static String reportPath = "/html/ysd/indexYs.html";//报告
    public static String qyjmPath = "/html/ysd/qyjm/qyjm_index.html";//签约居民
    public static String personPath = "/html/ysd/center/selfCenterYs.html";//个人
    public static String reservePath = "/html/ysd/wdyy/ysgrsy.html";//预约
    public static String yyghPath = "/html/ysd/wdyy/ysgrsy.html";//预约挂号
    public static String yyjcPath = "/html/ysd/jcyy/yyjc.html";//预约检查
    public static String yycwPath = "/html/ysd/yycw/yycw.html";//预约床位


    
    public static String loginUrl = hostIp + loginPath;//登录
    public static String reportUrl = hostIp + reportPath;//工作报告
    public static String reserveUrl = hostIp + reservePath;//预约
    public static String qyjmUrl = hostIp + qyjmPath;//签约居民
    public static String personUrl = hostIp + personPath;//个人

    public static String yyghUrl= hostIp + yyghPath;//预约挂号
    public static String yyjcUrl = hostIp + yyjcPath;//预约检查
    public static String yycwUrl = hostIp + yycwPath;//预约床位

}
